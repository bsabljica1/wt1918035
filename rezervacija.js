/*
document.getElementById("rezervacija").addEventListener("click", function () {
    Pozivi.pozivStranice("/rezervacija.html");
});

document.getElementById("pocetna").addEventListener("click", function () {
    Pozivi.pozivStranice("/pocetna.html");
});

document.getElementById("unos").addEventListener("click", function () {
    Pozivi.pozivStranice("/unos.html");
});

document.getElementById("sale").addEventListener("click", function () {
    Pozivi.pozivStranice("/sale.html");
});
*/

Pozivi.ucitajPodatke();
Pozivi.ucitajOsoblje();
Pozivi.ucitajSale();

let kal = document.getElementById("kalendar");
let dani = kal.getElementsByClassName("dan");
for (let i=0; i<dani.length; i++) {
    dani[i].addEventListener("click", function() {
        let brojDana = dani[i].innerText;
        if (brojDana == "")
        return;
        let con = confirm("Da li zelite rezervisati ovaj termin?");

        if (con === true) {
            const rezervacija = formiranaRezervacija(brojDana);
            if (rezervacija !== 0)
            Pozivi.rezervisanjeTermina(rezervacija);
        }
        });
}




function formiranaRezervacija(brojDana) {
    let naziv = document.getElementById("odabirSale").value;
        let period = document.getElementById("period").checked;
        let mjesec = document.getElementById("naziv_mjeseca").innerHTML;
        let pocetak = document.getElementById("pocetak").value;
        let kraj = document.getElementById("kraj").value;
        let godina = new Date().getFullYear();
        let osoba = document.getElementById("listaOsoblja").value;
        let rezultat = {};
        if (naziv == "Sala" || pocetak == "" || kraj == "" || osoba == "Osoblje") {
        alert("Podaci pri rezervisanju nedostaju.");
        return 0;
        }

        if (kraj<=pocetak) {
            alert("Odabrani vremenski interval nije validan.");
            return 0;
        }

        let mjesecBroj = -1;
        let semestar = "";
        if (mjesec === "Januar") mjesecBroj = 0;
        else if (mjesec === "Februar") mjesecBroj = 1;
        else if (mjesec === "Mart") mjesecBroj = 2;
        else if (mjesec === "April")mjesecBroj = 3;
        else if (mjesec === "Maj") mjesecBroj = 4;
        else if (mjesec === "Juni") mjesecBroj = 5;
        else if (mjesec === "Juli") mjesecBroj = 6;
        else if (mjesec === "August") mjesecBroj = 7;
        else if (mjesec === "Septembar") mjesecBroj = 8;
        else if (mjesec === "Oktobar") mjesecBroj = 9;
        else if (mjesec === "Novembar") mjesecBroj = 10;
        else if (mjesec === "Decembar") mjesecBroj = 11;

    if ((mjesecBroj >=9 && mjesecBroj <=11) || mjesecBroj ===0)
        semestar = "zimski";
    else if (mjesecBroj >=1 && mjesecBroj<=5)
        semestar = "ljetni";

    if (period)
        if (semestar == "") {
            alert("Nije moguce napraviti periodicnu rezervaciju za mjesec van ljetnog ili zimskog semestra.");
            return 0;
        }

        if (period)
            rezultat = {
                dan: event.target.cellIndex,
                semestar,
                pocetak,
                kraj,
                naziv,
                predavac: osoba.split(",")[0]
            };
        else {
            rezultat = {
                datum: (event.target.innerHTML.length === 1 ? "0" : "") +
                event.target.innerHTML +
                "." +
                (mjesecBroj < 9 ? "0" : "") +
                (mjesecBroj + 1) +
                "." +
                new Date().getFullYear(),
                pocetak,
                kraj,
                naziv,
                predavac: osoba.split(",")[0]
            };
        }
  return rezultat;
}


function popuniOsoblje(podaci) {
    let forma = document.getElementById("listaOsoblja");
    forma.innerHTML = podaci;
}

function popuniSale(podaci) {
    let forma = document.getElementById("odabirSale");
    forma.innerHTML = podaci;
}
