var Pozivi = (function(){

function ucitajPodatke() {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {
    if (ajax.readyState == 4 && ajax.status == 200) {
            const podaci = JSON.parse(ajax.responseText);
            Kalendar.ucitajPodatke(podaci['periodicna'], podaci['vanredna']);
        }
        if (ajax.readyState == 4 && ajax.status == 404)
            document.body.innerHTML = "Greska: nepoznat URL";
    }
    ajax.open("GET", "/zauzeca", true);
    ajax.send();
}


function rezervisanjeTermina(rezervacija) {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {
        if (ajax.readyState == 4 && ajax.status == 200) {
            podaci = JSON.parse(ajax.responseText);
            Kalendar.ucitajPodatke(podaci['periodicna'], podaci['vanredna']);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), odabraniMjesec, document.getElementById("odabirSale").value,
                document.getElementById("pocetak").value,
                document.getElementById("kraj").value);

        }
        if (ajax.readyState == 4 && ajax.status == 400) {
            alert(ajax.responseText);
            ucitajPodatke();
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), odabraniMjesec, document.getElementById("odabirSale").value,
                document.getElementById("pocetak").value,
                document.getElementById("kraj").value);
        }

        if (ajax.readyState == 4 && ajax.status == 404)
            document.body.innerHTML = "Greska: nepoznat URL";
    }

    ajax.open("POST", "/zauzeca", true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send(JSON.stringify(rezervacija));
}



function ucitajSlike(ucitane, slike) {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {
    if (ajax.readyState == 4 && ajax.status == 200) {
        let slikeUcitane = JSON.parse(ajax.responseText);
        slike(slikeUcitane);
    }
        if (ajax.readyState == 4 && ajax.status == 404)
        document.getElementById("galerija").innerHTML = "Greska: nepoznat URL";
    }
    ajax.open("POST", "/galerija", true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send(JSON.stringify({ucitane}));
}

function ucitajOsoblje() {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {
    if (ajax.readyState == 4 && ajax.status == 200) {
            const podaci = JSON.stringify(ajax.responseText);
            popuniOsoblje(podaci);
        }
        if (ajax.readyState == 4 && ajax.status == 404)
            document.body.innerHTML = "Greska: nepoznat URL";
    }
    ajax.open("GET", "/osoblje", true);
    ajax.send();
}

function ucitajSale() {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {
    if (ajax.readyState == 4 && ajax.status == 200) {
            const podaci = JSON.stringify(ajax.responseText);
            popuniSale(podaci);
        }
        if (ajax.readyState == 4 && ajax.status == 404)
            document.body.innerHTML = "Greska: nepoznat URL";
    }
    ajax.open("GET", "/sale", true);
    ajax.send();
}


function listaOsoblja() {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {
    if (ajax.readyState == 4 && ajax.status == 200) {
            const podaci = JSON.stringify(ajax.responseText);
            kreirajTabelu(podaci);
        }
        if (ajax.readyState == 4 && ajax.status == 404)
            document.body.innerHTML = "Greska: nepoznat URL";
    }
    ajax.open("GET", "/tabela", true);
    ajax.send();
}


return{
    ucitajPodatke: ucitajPodatke,
    rezervisanjeTermina: rezervisanjeTermina,
    ucitajSlike: ucitajSlike,
    ucitajOsoblje: ucitajOsoblje,
    ucitajSale: ucitajSale,
    listaOsoblja: listaOsoblja
}

}());