const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
var datumSpecijalni = new Date();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/html"));
app.use(express.static(__dirname + "/css"));
app.use(express.static(__dirname + "/js"));
app.use(express.static(__dirname + "/json"));

const zauzeca = require("./json/zauzeca.json");
const galerija = require("./json/galerija.json");
let pomZauzeca = {
    periodicna: [],
    vanredna: []
};
var brojStranice = 0;
var brojUcitanihSlika = 0;
var galerijaUcitanih = {"slike":[]}
let pomOsoba = "";


const sequelize = require('./js/db.js');
const Osoblje = sequelize.import(__dirname+"/js/osoblje.js");
const Termin = sequelize.import(__dirname+"/js/termin.js");
const Sala = sequelize.import(__dirname+"/js/sala.js");
const Rezervacija = sequelize.import(__dirname+"/js/rezervacije.js");


app.get("/",function(req,res){    
    res.redirect("/pocetna.html");
});

app.get("/zauzeca",function(req,res){
    pomZauzeca = {
        periodicna: [],
        vanredna: []
    };
    let rezultat ={};
    let predavac = "";
    let dan;
    let pocetak = "";
    let kraj = "";
    let datum = "";
    let naziv = "";
    let semestar = ""; 


       Rezervacija.findAll().then(
           rezervacije => {

            for (let i=0; i<rezervacije.length; i++) {

                Osoblje.findOne({where:{id: rezervacije[i].osoba}}).then(
                    predavac => {

                        predavac = predavac.ime + " " + predavac.prezime;
                        Sala.findOne({where:{id: rezervacije[i].sala}}).then(
                            naziv => {

                                naziv = naziv.naziv;
                                Termin.findOne({where:{id: rezervacije[i].termin}}).then(
                                    termini => {

                                        dan = termini.dan;
                                        datum = termini.datum;
                                        semestar = termini.semestar;
                                        let pom1 = termini.pocetak.split(":");
                                        pocetak = pom1[0] + ":" + pom1[1];
                                        let pom2 = termini.kraj.split(":");
                                        kraj = pom2[0] + ":" + pom2[1];

                                        if (dan != null) {
                                            rezultat = {
                                                dan,
                                                semestar,
                                                pocetak,
                                                kraj,
                                                naziv,
                                                predavac
                                            };
                                            pomZauzeca.periodicna.push(rezultat);
                                        }
                                        else {
                                            rezultat = {
                                                datum,
                                                pocetak,
                                                kraj,
                                                naziv,
                                                predavac
                                            };
                                            pomZauzeca.vanredna.push(rezultat);
                                        }

                                        if (i == rezervacije.length-1)
                                        res.json(pomZauzeca);

                                    }, err => {console.log(err);}
                                )

                            }, err => {console.log(err);}
                        )

                    }, err => {console.log(err);}
                )

            }

           }, err => { console.log(err);}
       );
});

app.get("/osoblje", function(req,res){
    let podaci = Osoblje.findAll();
    let forma ="";
    podaci.then(
        podaci => {
            forma += "<option selected disabled>Osoblje</option>";
            for (let i=0; i<podaci.length; i++) {
                forma += "<option>"+podaci[i].ime+" "+podaci[i].prezime+", "+podaci[i].uloga+"</option>";
            }
            res.end(forma);
        },
        error => {
            console.log(error);
            res.end(forma);
        }
    );

});

app.get("/sale", function(req,res){
    let podaci = Sala.findAll();
    let forma ="";
    podaci.then(
        podaci => {
            forma += "<option selected disabled>Sala</option>";
            for (let i=0; i<podaci.length; i++) {
                forma += "<option>"+podaci[i].naziv+"</option>";
            }
            res.end(forma);
        },
        error => {
            console.log(error);
            res.end(forma);
        }
    );

});

app.get("/tabela", async function(req,res){
    let status = "U kancelariji";
    let osobe = await Osoblje.findAll();
    let forma = "<table class='tabelaOsoba' id='osobe'><tr><th class='naslov'>Ime</th><th class='naslov'>Prezime</th><th class='naslov'>Uloga</th><th class='naslov'>Sala</th></tr>";
    for (let i=0; i<osobe.length; i++) {
        let status = "U kancelariji";
        let rezervacija = await Rezervacija.findAll({where: {osoba: osobe[i].id}});
        if (rezervacija != null) {
            for (let j=0; j<rezervacija.length; j++) {
            let termin = await Termin.findOne({where:{id: rezervacija[j].termin}});
            if (termin != null)
            if (uporediTrenutak(termin)) {
            let sala = await Sala.findOne({where:{id: rezervacija[j].sala}});
            status = sala.naziv;
            break;
            }
        }
        }
        forma += "<tr><td class='polje'>"+osobe[i].ime+"</td><td class='polje'>"+osobe[i].prezime+"</td><td class='polje'>"+osobe[i].uloga+"</td><td class='polje'>"+status+"</td></tr>";
    }
    forma += "</table>"
    res.end(forma);
});





app.post("/zauzeca",function(req,res){     
    let tijelo = req.body;

    pomZauzeca = {
        periodicna: [],
        vanredna: []
    };
    let rezultat ={};
    let predavac = "";
    let dan;
    let pocetak = "";
    let kraj = "";
    let datum = "";
    let naziv = "";
    let semestar = ""; 


       Rezervacija.findAll().then(
           rezervacije => {

            for (let i=0; i<rezervacije.length; i++) {

                Osoblje.findOne({where:{id: rezervacije[i].osoba}}).then(
                    predavac => {

                        predavac = predavac.ime + " " + predavac.prezime;
                        Sala.findOne({where:{id: rezervacije[i].sala}}).then(
                            naziv => {

                                naziv = naziv.naziv;
                                Termin.findOne({where:{id: rezervacije[i].termin}}).then(
                                    termini => {

                                        dan = termini.dan;
                                        datum = termini.datum;
                                        semestar = termini.semestar;
                                        let pom1 = termini.pocetak.split(":");
                                        pocetak = pom1[0] + ":" + pom1[1];
                                        let pom2 = termini.kraj.split(":");
                                        kraj = pom2[0] + ":" + pom2[1];

                                        if (dan != null) {
                                            rezultat = {
                                                dan,
                                                semestar,
                                                pocetak,
                                                kraj,
                                                naziv,
                                                predavac
                                            };
                                            pomZauzeca.periodicna.push(rezultat);
                                        }
                                        else {
                                            rezultat = {
                                                datum,
                                                pocetak,
                                                kraj,
                                                naziv,
                                                predavac
                                            };
                                            pomZauzeca.vanredna.push(rezultat);
                                        }

                                        if (i == rezervacije.length-1) {
                                            if (!validacijaPodatakaRezervacije(tijelo)) {
                                                res.writeHead(400, { "Content-type": "text/text" });
                                                res.end("Termin koji ste pokusali rezervisati nije validan!");
                                            }
                                            else if (!validacijaZauzecaRezervacije(tijelo)) {
                                                res.writeHead(400, { "Content-type": "text/text" });
                                                if (tijelo.dan != undefined) {
                                                res.end("Nije moguće rezervisati salu " + tijelo.naziv + " za navedeni datum " + datumSpecijalni + " i termin od " + tijelo.pocetak + " do " + tijelo.kraj + "!"
                                                + " Osoba koja je rezervisala salu je: " + pomOsoba + ".");
                                                }
                                                else
                                                res.end("Nije moguće rezervisati salu " + tijelo.naziv + " za navedeni datum " + tijelo.datum + " i termin od " + tijelo.pocetak + " do " + tijelo.kraj + "!"
                                                + " Osoba koja je rezervisala salu je: " + pomOsoba + ".");
                                            }
                                            else {
                                        
                                            pom1 = tijelo.pocetak.split(":");
                                            let poc = new Date(2020, 1, 1, Number(pom1[0])+1, Number(pom1[1]), 0, 0);
                                            pom2 = tijelo.kraj.split(":");
                                            let kr = new Date(2020, 1, 1, Number(pom2[0])+1, Number(pom2[1]), 0, 0);
                                            if (tijelo.semestar != null)
                                            Termin.create({redovni: true, dan: tijelo.dan, datum: null, semestar: tijelo.semestar, pocetak: poc, kraj: kr});
                                            else
                                            Termin.create({redovni: false, dan: null, datum: tijelo.datum, semestar: null, pocetak: poc, kraj: kr});
                                        
                                            let prezime = tijelo.predavac.split(" ")[1];
                                            Osoblje.findOne({where: 
                                                   {prezime: prezime} 
                                            }
                                            ).then(osoba => {
                                                Sala.findOne({where: { naziv: tijelo.naziv }}).then(sala => {
                                                    Termin.findAll().then(termini => {
                                                        console.log(JSON.stringify(termini[termini.length-1]));
                                                        console.log(JSON.stringify(sala));
                                                        console.log(JSON.stringify(osoba));
                                                        Rezervacija.create({termin: termini[termini.length-1].id, sala: sala.id, osoba: osoba.id});
                                                        if (tijelo.dan != null)
                                                        pomZauzeca.periodicna.push(tijelo);
                                                        else
                                                        pomZauzeca.vanredna.push(tijelo);
                                                        res.json(pomZauzeca);
                                                    })
                                                })
                                            });
                                        }
                                        }

                                    }, err => {console.log(err);}
                                )

                            }, err => {console.log(err);}
                        )

                    }, err => {console.log(err);}
                )

            }

           }, err => { console.log(err);}
       );
});




app.post("/galerija", function(req,res){
    let pom = req.body;
    if (pom.ucitane == 0) {
        brojStranice = 0;
        brojUcitanihSlika = 0;
        galerijaUcitanih = {"slike":[]}
    }
    brojUcitanihSlika = galerijaUcitanih.slike.length;
    let brojac = 3;
    if (brojUcitanihSlika > 7) {
        brojac = 10 - brojUcitanihSlika;
    }
    while (brojac > 0) {
        galerijaUcitanih.slike.push(galerija.slike[brojUcitanihSlika]);
        brojUcitanihSlika++;
        brojac--;
    }
    res.json(galerijaUcitanih);
});





app.get("/css/pocetna.css",function(req,res){        
    res.sendFile(__dirname + "/css/pocetna.css");
});

app.get("/pozivi.js",function(req,res){        
    res.sendFile(__dirname + "/pozivi.js");
});

app.get("/js/kalendar.js",function(req,res){
    res.sendFile(__dirname + "/js/kalendar.js");
});

app.get("/pocetna.js",function(req,res){        
    res.sendFile(__dirname + "/pocetna.js");
});

app.get("/unos.js",function(req,res){        
    res.sendFile(__dirname + "/unos.js");
});

app.get("/rezervacija.js",function(req,res){        
    res.sendFile(__dirname + "/rezervacija.js");
});

app.get("/osobe.js",function(req,res){        
    res.sendFile(__dirname + "/osobe.js");
});

app.get("/sale.js",function(req,res){        
    res.sendFile(__dirname + "/sale.js");
});

app.get("/rezervacija.html",function(req,res){        
    res.sendFile(__dirname + "/html/rezervacija.html");
});

app.get("/css/rezervacija.css",function(req,res){        
    res.sendFile(__dirname + "/css/rezervacija.css");
});

app.get("/unos.html",function(req,res){        
    res.sendFile(__dirname + "/html/unos.html");
});

app.get("/css/unos.css",function(req,res){        
    res.sendFile(__dirname + "/css/unos.css");
});

app.get("/sale.html",function(req,res){        
    res.sendFile(__dirname + "/html/sale.html");
});

app.get("/css/sale.css",function(req,res){        
    res.sendFile(__dirname + "/css/sale.css");
});

app.get("/osoblje.html",function(req,res){        
    res.sendFile(__dirname + "/html/osoblje.html");
});

app.get("/css/osoblje.css",function(req,res){        
    res.sendFile(__dirname + "/css/osoblje.css");
});

app.listen(8080);


function uporediTrenutak(termin) {
    console.log("ULAZAK U UPOREDI TERMIN");
    let pocetak = termin.pocetak;
    let kraj = termin.kraj;
    if (termin.semestar != null) {
        console.log("TERMIN JE PERIODICNI");
        let danUSedmici = new Date().getDay();
        if (danUSedmici === 0)
            danUSedmici =6;
        else
            danUSedmici--;
        let semestar = termin.semestar;
        let pom ="";
        console.log("ISPIS TRENUGNOG MJESECA: " + new Date().getMonth());
        if (new Date().getMonth() >8 || new Date().getMonth() ===0)
        pom = "zimski";
        else if (new Date().getMonth() >0 && new Date().getMonth()<6)
        pom = "ljetni";
        let vrijeme = new Date().getHours() + ":" + new Date().getMinutes();
        console.log("ISPIS SVIH VRIJEDNOSTI (TRENUTNO, BAZNO):");
        console.log("VRIJEME: " + vrijeme + ", POCETAK: " + pocetak + ", KRAJ: " + kraj);
        console.log("DAN U SEDMICI" + danUSedmici + ", " + termin.dan);
        console.log("SEMESTAR: " + pom + ", " + semestar);
        if (vrijeme >= pocetak && vrijeme <= kraj) {
            console.log("DOBRO VRIJEME");
        if (danUSedmici == termin.dan) {
            console.log("DOBAR DAN");
        if (pom == semestar) {
            console.log("DOBAR SEMESTAR");
        console.log("IMA PODUDARANJE SA PERIODICNOM");
        return true;
        } 
    } 
}
console.log("NEMA PODUDARANJE SA PERIODICNOM");
return false;
    }
    else {
    let vrijeme = new Date().getHours() + ":" + new Date().getMinutes();
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0');
    let yyyy = today.getFullYear();
    today = dd + '.' + mm + '.' + yyyy;
    console.log("VRIJEME: " + vrijeme + ", POCETAK: " + pocetak + ", KRAJ: " + kraj);
    console.log("DATUM: " + today + ", " + termin.datum);
    if (vrijeme >= pocetak && vrijeme <= kraj) {
        console.log("DOBRO VRIJEME");
        if (today == termin.datum) {
            console.log("DOBAR TERMIN");
            return true;
        }
    }
    return false;
    }
}




function validacijaZauzecaRezervacije(tijelo) {
    if (tijelo.dan !== undefined) {
        if (!validacijaZauzecaPeriodicnog(tijelo))
            return false;
        return true;
    }
    else {
        if (!validacijaZauzecaVanrednog(tijelo))
            return false;
        return true;
    }
}



function validacijaZauzecaPeriodicnog(tijelo) {
    let listaPeriodicnih = pomZauzeca.periodicna;

    for (let i=0; i < listaPeriodicnih.length; i++) {
        if (listaPeriodicnih[i].dan === tijelo.dan && listaPeriodicnih[i].semestar === tijelo.semestar && 
            listaPeriodicnih[i].naziv === tijelo.naziv && ((listaPeriodicnih[i].pocetak < tijelo.kraj && listaPeriodicnih[i].kraj > tijelo.pocetak)
            || (listaPeriodicnih[i].kraj > tijelo.pocetak && listaPeriodicnih[i].pocetak < tijelo.kraj))) {
                datumSpecijalni = "";
                pomOsoba = listaPeriodicnih[i].predavac;
                 return false;
            }
    }

    let listaVanrednih = pomZauzeca.vanredna;
    for (let i=0; i < listaVanrednih.length; i++) {
        let datumNiz = listaVanrednih[i].datum.split(".");
        datumSpecijalni = new Date(datumNiz[1] + " " + datumNiz[0] + " " + datumNiz[2]);
        let pom = "";
        if (datumSpecijalni.getMonth() >8 || datumSpecijalni.getMonth() ===0)
        pom = "zimski";
        else if (datumSpecijalni.getMonth() >0 && datumSpecijalni.getMonth()<6)
        pom = "ljetni";
        let danUSedmici = datumSpecijalni.getDay() === 0 ? 6 : datumSpecijalni.getDay() - 1;
        if (pom === tijelo.semestar && listaVanrednih[i].naziv === tijelo.naziv && danUSedmici === tijelo.dan &&
            ((listaVanrednih[i].pocetak < tijelo.kraj && listaVanrednih[i].kraj > tijelo.pocetak)
            || (listaVanrednih[i].kraj > tijelo.pocetak && listaVanrednih[i].pocetak < tijelo.kraj))) {
                datumSpecijalni = listaVanrednih[i].datum;
                pomOsoba = listaVanrednih[i].predavac;
                 return false;
            }
    }
    return true;
}


function validacijaZauzecaVanrednog(tijelo) {
    let listaVanrednih = pomZauzeca.vanredna;

    for (let i=0; i < listaVanrednih.length; i++) {
        if (listaVanrednih[i].datum === tijelo.datum && listaVanrednih[i].naziv === tijelo.naziv && 
            ((listaVanrednih[i].pocetak < tijelo.kraj && listaVanrednih[i].kraj > tijelo.pocetak)
            || (listaVanrednih[i].kraj > tijelo.pocetak && listaVanrednih[i].pocetak < tijelo.kraj))) {
                pomOsoba = listaVanrednih[i].predavac;
                return false;}
    }

    let listaPeriodicnih = pomZauzeca.periodicna;

    for (let i=0; i < listaPeriodicnih.length; i++) {
        let datumNiz = tijelo.datum.split(".");
        let datum = new Date(datumNiz[1] + " " + datumNiz[0] + " " + datumNiz[2]);
        if (datum.getMonth() > 5 && datum.getMonth() < 9) 
            return true;
        let dan = datum.getDay() === 0 ? 6 : datum.getDay() - 1;
        let pom = "";
        if (datum.getMonth() >8 || datum.getMonth() ===0)
        pom = "zimski";
        else if (datum.getMonth() >0 && datum.getMonth()<6)
        pom = "ljetni";
        if (listaPeriodicnih[i].semestar === pom && listaPeriodicnih[i].naziv === tijelo.naziv && listaPeriodicnih[i].dan === dan &&
            ((listaPeriodicnih[i].pocetak < tijelo.kraj && listaPeriodicnih[i].kraj > tijelo.pocetak)
            || (listaPeriodicnih[i].kraj > tijelo.pocetak && listaPeriodicnih[i].pocetak < tijelo.kraj))) {
                pomOsoba = listaPeriodicnih[i].predavac;
                return false;}
    }

    return true;
}













function validacijaPodatakaRezervacije(tijelo) {

    if (tijelo.dan !== undefined) {
        if (!validirajDan(tijelo.dan))
            return false;
        if (!validirajVrijemePocetakKraj(tijelo.pocetak, tijelo.kraj))
            return false;
        if (!validirajSemestar(tijelo.semestar))
            return false;
        if (!validirajSalu(tijelo.naziv))
            return false;
    }
    else {
        if (!validirajDatum(tijelo.datum))
            return false;
        if (!validirajVrijemePocetakKraj(tijelo.pocetak, tijelo.kraj))
            return false;
        if (!validirajSalu(tijelo.naziv))
            return false;
    }
    return true;
}





function validirajDatum (datum) {
    if (typeof datum !== "string") return false;
    if (
        !datum.match(
            /^([0-2][0-9]|(3)[0-1])(\.)(((0)[0-9])|((1)[0-2]))(\.)\d{4}$/i
        )
    )
        return false;
    return true;
}


function validirajVrijemePocetakKraj (pocetak, kraj) {
    if (pocetak === "undefined" || kraj === "undefined") return false;
    if (typeof pocetak !== "string") return false;
    if (!pocetak.match(/^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$/i)) return false;
    if (typeof kraj !== "string") return false;
    if (!kraj.match(/^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$/i)) return false;
    if (kraj <= pocetak) return false;
    return true;
}

function validirajMjesec (mjesec) {
    if (mjesec<0 || mjesec>11) return false;
    return true;
}

function validirajDan (dan) {
    if (dan<0 || dan>6) return false;
    return true;
}

function validirajSemestar (semestar) {
        if  (semestar === "zimski") return true;
        if  (semestar === "ljetni") return true;
        return false;
}

function validirajSalu (sala) {
    if (sala === "undefined") return false;
    if (typeof sala !== "string") return false;
    /*
    if (sala === "VA1") return true;
    if (sala === "VA2") return true;
    if (sala === "MA") return true;
    if (sala === "EE1") return true;
    if (sala === "EE2") return true;
    if (sala === "0-01") return true;
    if (sala === "0-02") return true;
    if (sala === "0-03") return true;
    if (sala === "0-04") return true;
    if (sala === "0-05") return true;
    if (sala === "0-06") return true;
    if (sala === "0-07") return true;
    if (sala === "0-08") return true;
    if (sala === "0-09") return true;
    if (sala === "1-01") return true;
    if (sala === "1-02") return true;
    if (sala === "1-03") return true;
    if (sala === "1-04") return true;
    if (sala === "1-05") return true;
    if (sala === "1-06") return true;
    if (sala === "1-07") return true;
    if (sala === "1-08") return true;
    if (sala === "1-09") return true;
    return false;
    */
   return true;
}


module.exports = app;