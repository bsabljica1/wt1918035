const supertest = require("supertest");
const assert = require('assert');
const app = require("../index");
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const expect = chai.expect;
chai.should();

describe("GET '/osoblje'", function() {

  beforeEach(done => {
    setTimeout(() => {
      done();
    }, 1500);
  });

    it("Provjerava da li uspjesno izvrsava get zahtjev, tj. da li ce vratiti status code 200", function(done) {
      supertest(app)
        .get("/osoblje")
        .expect(200)
        .end(function(err, res){
          if (err) done(err);
          done();
        });
    });

    it("Provjerava da li je ispravno prikupljeno osoblje", function(done) {
        supertest(app)
          .get("/osoblje")
          .expect(200)
          .end(function(err, res){
            if (err) done(err);
            let tacno = true;
            let vracenoOsoblje = res.body;
            let ocekivanoOsoblje = ["Neko", "Drugi", "Test"];
            for (let i=0; i<vracenoOsoblje.length; i++) {
                for (let j=0; j<ocekivanoOsoblje.length; j++) {
                    if (ocekivanoOsoblje[j] != vracenoOsoblje[i].ime) {
                        tacno = false;
                        break;
                    }
                }
            }
            expect(tacno).to.be.true;
            done();
          });
      });

  });


  describe("GET '/zauzeca'", function(){

    beforeEach(done => {
      setTimeout(() => {
        done();
      }, 1500);
    });

    it("Provjerava da li su ispravno prikupljena zauzeca, gledajuci broj prikupljenih periodicnih i vanrednih", function(done) {
      supertest(app)
        .get("/zauzeca")
        .expect(200)
        .end(function(err, res){
          if (err) done(err);
          let tacno = false;
          let vracenaZauzeca = res.body;
          vracenaZauzeca.should.have.property("periodicna");
          vracenaZauzeca.should.have.property("vanredna");
          if (vracenaZauzeca.periodicna.length == 1 && vracenaZauzeca.vanredna.length == 1)
          tacno = true;
          expect(tacno).to.be.true;
          done();
        });
    });

  });


  describe("GET '/sale'", function(){

    beforeEach(done => {
      setTimeout(() => {
        done();
      }, 1500);
    });

    it("Provjerava da li su ispravno prikupljene sale", function(done) {
      supertest(app)
        .get("/sale")
        .expect(200)
        .end(function(err, res){
          if (err) done(err);
            let tacno = true;
            let vraceneSale = res.body;
            let ocekivaneSale = ["1-11", "1-15"];
            for (let i=0; i<vraceneSale.length; i++) {
                for (let j=0; j<ocekivaneSale.length; j++) {
                    if (ocekivaneSale[j] != vraceneSale[i].naziv) {
                        tacno = false;
                        break;
                    }
                }
            }
            expect(tacno).to.be.true;
            done();
        });
    });

  });


  describe("GET '/tabela'", function(){

    beforeEach(done => {
      setTimeout(() => {
        done();
      }, 1500);
    });

    it("Provjerava da li je ispravno prikupljeno osoblje sa statusom koje se koristi za ispis tabela na stranici Osoblje", function(done) {
      supertest(app)
        .get("/tabela")
        .expect(200)
        .end(function(err, res){
          if (err) done(err);
            let tacno = true;
            let vraceniPodaci = res.body;
            let ocekivaniPodaci = [
              {ime: 'Neko', prezime: 'Nekic', uloga: 'profesor'},
              {ime: 'Drugi', prezime: 'Neko', uloga: 'asistent'},
              {ime: 'Test', prezime: 'Test', uloga: 'asistent'}
            ];
            for (let i=0; i<vraceniPodaci.length; i++) {
                for (let j=0; j<ocekivaniPodaci.length; j++) {
                    if (ocekivaniPodaci[j].ime != vraceniPodaci[i].ime || ocekivaniPodaci[j].prezime != vraceniPodaci[i].prezime || ocekivaniPodaci[j].uloga != vraceniPodaci[i].uloga) {
                        tacno = false;
                        break;
                    }
                }
            }
            expect(tacno).to.be.true;
            done();
        });
    });

  });


  describe("POST '/zauzeca'", function(){

    beforeEach(done => {
      setTimeout(() => {
        done();
      }, 1500);
    });

    it("Provjerava da li se registruje greska ako se posalje nepotpuna periodicna rezervacija, ocekuje se status 400", function(done) {
      supertest(app)
        .post("/zauzeca")
        .send({
          dan: 1,
          pocetak: "13:00",
          kraj: "15:00",
          naziv: "1-11",
          predavac: "Test Test"
        })
        .expect(400)
        .end(function(err, res){
          if (err) done(err);
            done();
        });
    });


    it("Provjerava da li se registruje greska ako se posalje nepotpuna vanredna rezervacija, ocekuje se status 400", function(done) {
      supertest(app)
        .post("/zauzeca")
        .send({
          datum: "21.04.2020",
          pocetak: "13:00",
          naziv: "1-11",
          predavac: "Test Test"
        })
        .expect(400)
        .end(function(err, res){
          if (err) done(err);
            done();
        });
    });


    it("Provjerava da li se registruje greska ako se posalje vanredna rezervacija koja se preklapa sa postojecim vanrednim terminom, ocekuje se status 400", function(done) {
      supertest(app)
        .post("/zauzeca")
        .send({
          datum: "01.01.2020",
          pocetak: "08:00",
          kraj: "20:00",
          naziv: "1-11",
          predavac: "Test Test"
        })
        .expect(400)
        .end(function(err, res){
          if (err) done(err);
            done();
        });
    });


    it("Provjerava da li se registruje greska ako se posalje vanredna rezervacija koja se preklapa sa postojecim periodicnim terminom, ocekuje se status 400", function(done) {
      supertest(app)
        .post("/zauzeca")
        .send({
          datum: "06.01.2020",
          pocetak: "08:00",
          kraj: "20:00",
          naziv: "1-11",
          predavac: "Test Test"
        })
        .expect(400)
        .end(function(err, res){
          if (err) done(err);
            done();
        });
    });


    it("Provjerava da li se registruje greska ako se posalje periodicna rezervacija koja se preklapa sa postojecim periodicnim terminom, ocekuje se status 400", function(done) {
      supertest(app)
        .post("/zauzeca")
        .send({
          dan: 0,
          semestar: "zimski",
          pocetak: "08:00",
          kraj: "20:00",
          naziv: "1-11",
          predavac: "Test Test"
        })
        .expect(400)
        .end(function(err, res){
          if (err) done(err);
            done();
        });
    });


    it("Provjerava da li se registruje greska ako se posalje periodicna rezervacija koja se preklapa sa postojecim vanrednim terminom, ocekuje se status 400", function(done) {
      supertest(app)
        .post("/zauzeca")
        .send({
          dan: 2,
          semestar: "zimski",
          pocetak: "08:00",
          kraj: "20:00",
          naziv: "1-11",
          predavac: "Test Test"
        })
        .expect(400)
        .end(function(err, res){
          if (err) done(err);
            done();
        });
    });


    it("Provjerava da li se nova vanredna rezervacija upisuje u zauzeca", function(done) {
      supertest(app)
        .post("/zauzeca")
        .send({
          datum: "21.03.2020",
          pocetak: "08:00",
          kraj: "20:00",
          naziv: "1-11",
          predavac: "Test Test"
        })
        .expect(200)
        .end(function(err, res){
          if (err) done(err);
          let tacno = true;
          let ocekivanaNova = {
          datum: "21.03.2020",
          pocetak: "08:00",
          kraj: "20:00",
          naziv: "1-11",
          predavac: "Test Test"
          }
          let zauzeca = res.body;
          zauzeca.should.have.property("vanredna");
          zauzeca.should.have.property("periodicna");
          zauzeca.vanredna.should.have.length(2);
          if (zauzeca.vanredna[1].datum != ocekivanaNova.datum || zauzeca.vanredna[1].pocetak != ocekivanaNova.pocetak || 
            zauzeca.vanredna[1].kraj != ocekivanaNova.kraj || zauzeca.vanredna[1].naziv != ocekivanaNova.naziv || zauzeca.vanredna[1].predavac != ocekivanaNova.predavac)
            tacno = false;
          expect(tacno).to.be.true;
            done();
        });
    });


    it("Provjerava da li se nova periodicna rezervacija upisuje u zauzeca", function(done) {
      supertest(app)
        .post("/zauzeca")
        .send({
          dan: 6,
          semestar: "zimski",
          pocetak: "08:00",
          kraj: "20:00",
          naziv: "1-15",
          predavac: "Test Test"
        })
        .expect(200)
        .end(function(err, res){
          if (err) done(err);
          let tacno = true;
          let ocekivanaNova = {
            dan: 6,
            semestar: "zimski",
            pocetak: "08:00",
            kraj: "20:00",
            naziv: "1-15",
            predavac: "Test Test"
          }
          let zauzeca = res.body;
          zauzeca.should.have.property("vanredna");
          zauzeca.should.have.property("periodicna");
          zauzeca.periodicna.should.have.length(2);
          if (zauzeca.periodicna[1].dan != ocekivanaNova.dan || zauzeca.periodicna[1].pocetak != ocekivanaNova.pocetak || 
            zauzeca.periodicna[1].kraj != ocekivanaNova.kraj || zauzeca.periodicna[1].naziv != ocekivanaNova.naziv || zauzeca.periodicna[1].predavac != ocekivanaNova.predavac
            || zauzeca.periodicna[1].semestar != ocekivanaNova.semestar)
            tacno = false;
          expect(tacno).to.be.true;
            done();
        });
    });

  });