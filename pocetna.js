let broj = 0;
var slike = {"slike":[]}
var brojStranice = 1;

document.getElementById("prethodni").disabled = true;
Pozivi.ucitajSlike(broj, s => {
    for (let i=0; i<s.slike.length; i++)
    slike.slike.push(s.slike[i]);
    postaviNaStranicu(slike);
    broj++;
});


document.getElementById("sljedeci").addEventListener("click", function () {
    brojStranice++;
    if (brojStranice === 4)
        document.getElementById("sljedeci").disabled = true;
    else if (brojStranice === 2) 
        document.getElementById("prethodni").disabled = false;
        ukloniSaStranice();
    if (slike.slike.length != 10 && (slike.slike.length < brojStranice * 3 || (slike.slike.length === 9 && brojStranice === 4))) {
        Pozivi.ucitajSlike(broj, s => {
            slike = {"slike":[]}
            for (let i=0; i<s.slike.length; i++)
            slike.slike.push(s.slike[i]);
            postaviNaStranicu(slike);
        });
}
    else {
        postaviNaStranicu(slike);
    }
});


document.getElementById("prethodni").addEventListener("click", function () {
    brojStranice--;
    if (brojStranice === 1)
        document.getElementById("prethodni").disabled = true;
    else if (brojStranice === 3) 
        document.getElementById("sljedeci").disabled = false;
        ukloniSaStranice();
        postaviNaStranicu(slike);
});



function postaviNaStranicu(galerija) {
    let brojac = 3;
    let pom = 0;
    let brojUcitanih = 0;
    pom = brojStranice*brojac;
    while (brojac>0) {
      var img = document.createElement("img");
      img.classList.add("slika");
      if (galerija.slike[pom-brojac] == undefined)
        break;
      img.src = galerija.slike[pom-brojac];
      img.alt = "Slika " + (pom-brojac+1);
      document.getElementById("galerija").appendChild(img);
      brojac--;
      brojUcitanih++;
    }
  }


  function ukloniSaStranice() {
    let s = document.getElementsByClassName("slika");
    while (s.length > 0) s[0].remove();
  }
