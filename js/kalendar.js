var odabraniMjesec = new Date().getMonth();

let Kalendar = (function() {

    let periodicnaLista = [];
    let vanrednaLista = [];


    function validirajDatum (datum) {
        if (typeof datum !== "string") return false;
        if (
            !datum.match(
                /^([0-2][0-9]|(3)[0-1])(\.)(((0)[0-9])|((1)[0-2]))(\.)\d{4}$/i
            )
        )
            return false;
        return true;
    }


    function validirajVrijemePocetakKraj (pocetak, kraj) {
        if (pocetak === "undefined" || kraj === "undefined") return false;
        if (typeof pocetak !== "string") return false;
        if (!pocetak.match(/^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$/i)) return false;
        if (typeof kraj !== "string") return false;
        if (!kraj.match(/^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$/i)) return false;
        if (kraj <= pocetak) return false;
        return true;
    }

    function validirajMjesec (mjesec) {
        if (mjesec<0 || mjesec>11) return false;
        return true;
    }

    function validirajDan (dan) {
        if (dan<0 || dan>6) return false;
        return true;
    }

    function validirajSemestar (semestar) {
            if  (semestar === "zimski") return true;
            if  (semestar === "ljetni") return true;
            return false;
    }

    function validirajSalu (sala) {
        if (sala === "undefined") return false;
        if (typeof sala !== "string") return false;
        /*
        if (sala === "VA1") return true;
        if (sala === "VA2") return true;
        if (sala === "MA") return true;
        if (sala === "EE1") return true;
        if (sala === "EE2") return true;
        if (sala === "0-01") return true;
        if (sala === "0-02") return true;
        if (sala === "0-03") return true;
        if (sala === "0-04") return true;
        if (sala === "0-05") return true;
        if (sala === "0-06") return true;
        if (sala === "0-07") return true;
        if (sala === "0-08") return true;
        if (sala === "0-09") return true;
        if (sala === "1-01") return true;
        if (sala === "1-02") return true;
        if (sala === "1-03") return true;
        if (sala === "1-04") return true;
        if (sala === "1-05") return true;
        if (sala === "1-06") return true;
        if (sala === "1-07") return true;
        if (sala === "1-08") return true;
        if (sala === "1-09") return true;
        return false;
        */
       return true;
    }


    function obojiZauzeca (kalendarRef, mjesec, sala, pocetak, kraj) {

        console.log("Unisli smo");

        if (validirajMjesec(mjesec) === false || validirajSalu(sala) === false ||
            validirajVrijemePocetakKraj(pocetak, kraj) === false) {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), odabraniMjesec);
            return;
            }

            console.log("Tu smo nakon validacije");
            console.log("Ispis listi u kalendar.js: ");
            console.log(periodicnaLista);
            console.log(vanrednaLista);

        var dani = kalendarRef.getElementsByClassName("dan");

        for (var i=0; i<dani.length; i++) {

            if (dani[i].classList.contains("zauzeta")) {
                dani[i].classList.remove("zauzeta");
                dani[i].classList.add("slobodna");
            }

        }

        let periodicneSaleZauzeca = [];
        let vanredneSaleZauzeca = [];
        let semestar = "";

        if ((mjesec >=9 && mjesec <=11) || mjesec ===0)
            semestar = "zimski";
        else if (mjesec >=1 && mjesec<=5)
            semestar = "ljetni";

        for (let i=0; i<periodicnaLista.length; i++) {
            if (periodicnaLista[i].semestar === semestar && periodicnaLista[i].naziv === sala)
            if ((periodicnaLista[i].pocetak < kraj && periodicnaLista[i].kraj > pocetak) ||
                (periodicnaLista[i].kraj > pocetak && periodicnaLista[i].pocetak < kraj))
                periodicneSaleZauzeca.push(periodicnaLista[i]);
        }


        for (let i=0; i<vanrednaLista.length; i++) {
            let vanredniMjesec = vanrednaLista[i].datum.split(".");
            if (vanrednaLista[i].naziv === sala && (vanredniMjesec[1]-1)===mjesec)
            if ((vanrednaLista[i].pocetak < kraj && vanrednaLista[i].kraj > pocetak) ||
                (vanrednaLista[i].kraj > pocetak && vanrednaLista[i].pocetak < kraj))
            vanredneSaleZauzeca.push(vanrednaLista[i]);
        }

        for (let i=0; i<dani.length; i++) {

            for (let j=0; j<vanredneSaleZauzeca.length; j++) {
                let pom = vanredneSaleZauzeca[j].datum.split(".")[0];
                if (vanredneSaleZauzeca[j].datum.split(".")[0].length > 1)
                if (vanredneSaleZauzeca[j].datum.split(".")[0][0] == 0)
                    pom = vanredneSaleZauzeca[j].datum.split(".")[0][1];
                if (dani[i].innerText === pom) {
                    if (dani[i].classList.contains("slobodna")) {
                        dani[i].classList.remove("slobodna");
                        dani[i].classList.add("zauzeta");
                    }
                }
            }
        }

        for (let i=0; i<periodicneSaleZauzeca.length; i++) {
            let j= periodicneSaleZauzeca[i].dan;
            while (j < dani.length) {

                if (dani[j].classList.contains("slobodna")) {
                    dani[j].classList.remove("slobodna");
                    dani[j].classList.add("zauzeta");
                }

                j += 7;

            }
        }
   
    }




    function ucitajPodatke (periodicna, vanredna) {
        vanrednaLista = [];
        periodicnaLista = [];
        if (vanredna !== undefined) {
        for (let i=0; i<vanredna.length; i++) {
        if ( validirajDatum(vanredna[i].datum) === false ||
            validirajVrijemePocetakKraj (vanredna[i].pocetak, vanredna[i].kraj) === false ||
            validirajSalu(vanredna[i].naziv) === false || (typeof vanredna[i].predavac !== "string"))
            continue;
        else
            vanrednaLista.push(vanredna[i]);
            }
        }

        if (periodicna !== undefined) {
            for (let i=0; i<periodicna.length; i++) {
            if ( validirajDan(periodicna[i].dan) === false ||
                validirajSemestar(periodicna[i].semestar) === false ||
                validirajVrijemePocetakKraj (periodicna[i].pocetak, periodicna[i].kraj) === false ||
                validirajSalu(periodicna[i].naziv) === false || (typeof periodicna[i].predavac !== "string"))
                continue;
            else
                periodicnaLista.push(periodicna[i]);
                }
            }


    }




    function iscrtajKalendar (kalendarRef, mjesec) {

        if (validirajMjesec(mjesec)) {

        let firstDay = new Date(2020, mjesec, 1).getDay();

        if (firstDay === 0)
            firstDay =6;
        else
            firstDay--;
        
            let lastDay = new Date(2020, mjesec+ 1, 0).getDate();
        let dan = 0;


        var nazivMjeseca = document.getElementById("naziv_mjeseca");
        var dani = kalendarRef.getElementsByClassName("dan");

        if (mjesec === 0) nazivMjeseca.innerText = "Januar";
        else if (mjesec === 1) nazivMjeseca.innerText = "Februar";
        else if (mjesec === 2) nazivMjeseca.innerText = "Mart";
        else if (mjesec === 3) nazivMjeseca.innerText = "April";
        else if (mjesec === 4) nazivMjeseca.innerText = "Maj";
        else if (mjesec === 5) nazivMjeseca.innerText = "Juni";
        else if (mjesec === 6) nazivMjeseca.innerText = "Juli";
        else if (mjesec === 7) nazivMjeseca.innerText = "August";
        else if (mjesec === 8) nazivMjeseca.innerText = "Septembar";
        else if (mjesec === 9) nazivMjeseca.innerText = "Oktobar";
        else if (mjesec === 10) nazivMjeseca.innerText = "Novembar";
        else if (mjesec === 11) nazivMjeseca.innerText = "Decembar";

        for (let i=0; i<dani.length; i++) {

            dani[i].innerText = null;

            if (dani[i].classList.contains("slobodna"))         
                dani[i].classList.remove("slobodna");
        
            if (dani[i].classList.contains("zauzeta")) 
                dani[i].classList.remove("zauzeta");

            if ( firstDay === i ||  ( i>=firstDay && dan < lastDay)) {
                dan++;
                dani[i].innerText = dan;
                dani[i].classList.add("slobodna");
        }
    }

    }
    }

    return {
        obojiZauzeca: obojiZauzeca,
        ucitajPodatke: ucitajPodatke,
        iscrtajKalendar: iscrtajKalendar
    };

}());


/*
Kalendar.ucitajPodatke(
    [
        {
            dan: 0,
            semestar: "zimski",
            pocetak: "12:00",
            kraj: "13:30",
            naziv: "MA",
            predavac: "Ime1"
        },
        {
            dan: 1,
            semestar: "ljetni",
            pocetak: "14:00",
            kraj: "15:30",
            naziv: "EE1",
            predavac: "Ime2"
        },
        {
            dan: 4,
            semestar: "zimski",
            pocetak: "10:00",
            kraj: "11:00",
            naziv: "MA",
            predavac: "Ime3"
        }
    ],
    [
        {
            datum: "20.11.2019",
            pocetak: "11:00",
            kraj: "12:00",
            naziv: "0-01",
            predavac: "Ime4"
        },
        {
            datum: "15.11.2019",
            pocetak: "08:00",
            kraj: "09:30",
            naziv: "MA",
            predavac: "Ime5"
        },
        {
            datum: "21.11.2019",
            pocetak: "18:00",
            kraj: "19:00",
            naziv: "0-01",
            predavac: "Ime7"
        }
    ]
);
*/



Kalendar.iscrtajKalendar(document.getElementById("kalendar"), odabraniMjesec);


document.getElementById("odabirSale").addEventListener("change", function () {

    Kalendar.obojiZauzeca(document.getElementById("kalendar"), odabraniMjesec, 
    document.getElementById("odabirSale").options[document.getElementById("odabirSale").selectedIndex].text,
    document.getElementById("pocetak").value, document.getElementById("kraj").value);

});


document.getElementById("pocetak").addEventListener("change", function () {

    Kalendar.obojiZauzeca(document.getElementById("kalendar"), odabraniMjesec, 
    document.getElementById("odabirSale").options[document.getElementById("odabirSale").selectedIndex].text,
    document.getElementById("pocetak").value, document.getElementById("kraj").value);
    
});


document.getElementById("kraj").addEventListener("change", function () {

    Kalendar.obojiZauzeca(document.getElementById("kalendar"), odabraniMjesec, 
    document.getElementById("odabirSale").options[document.getElementById("odabirSale").selectedIndex].text,
    document.getElementById("pocetak").value, document.getElementById("kraj").value);
    
});

if (odabraniMjesec=== 11) 
document.getElementById("sljedeci").disabled = true;
if (odabraniMjesec === 0) 
document.getElementById("prethodni").disabled = true;

document.getElementById("prethodni").addEventListener("click", function() { 

    if (odabraniMjesec!=0)
    odabraniMjesec--;

    if (odabraniMjesec===0)
        document.getElementById("prethodni").disabled = true;
    else if (odabraniMjesec === 10) 
        document.getElementById("sljedeci").disabled = false;

    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), odabraniMjesec);

    Kalendar.obojiZauzeca(document.getElementById("kalendar"), odabraniMjesec, 
    document.getElementById("odabirSale").options[document.getElementById("odabirSale").selectedIndex].text,
    document.getElementById("pocetak").value, document.getElementById("kraj").value);

});


document.getElementById("sljedeci").addEventListener("click", function () {

    odabraniMjesec++;

    if (odabraniMjesec=== 11) 
        document.getElementById("sljedeci").disabled = true;
    else if (odabraniMjesec === 1) 
        document.getElementById("prethodni").disabled = false;

    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), odabraniMjesec);

    Kalendar.obojiZauzeca(document.getElementById("kalendar"), odabraniMjesec, 
    document.getElementById("odabirSale").options[document.getElementById("odabirSale").selectedIndex].text,
    document.getElementById("pocetak").value, document.getElementById("kraj").value);

});