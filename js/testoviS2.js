let assert = chai.assert;
describe('Kalendar', function() {

 describe('iscrtajKalendar()', function() {

   it('Test za iscrtavanje kalendara za mjesec koji ima 30 dana (Septembar)', function() {
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 8);
     let dani = document.getElementsByClassName("dan");
     let broj = 0;
     for (let i=0; i<dani.length; i++) {
         if (dani[i].classList.contains("slobodna"))
            broj++;
     }
     assert.equal(broj, 30,"Broj dana treba biti 30");
   });


   it('Test za iscrtavanje kalendara za mjesec koji ima 31 dan (Decembar)', function() {
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 11);
     let dani = document.getElementsByClassName("dan");
     let broj = 0;
     for (let i=0; i<dani.length; i++) {
         if (dani[i].classList.contains("slobodna"))
            broj++;
     }
     assert.equal(broj, 31,"Broj dana treba biti 31");
   });


   it('Test za iscrtavanje kalendara za trenutni mjesec (Novembar), uz provjeru koji je dan prvi u mjesecu', function() {
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), new Date().getMonth());
     let dani = document.getElementsByClassName("dan");
     let broj = 0;
     for (let i=0; i<dani.length; i++) {
         if (dani[i].classList.contains("slobodna"))
            broj++;
     }
     assert.equal(broj, 30,"Ocekivano 30 dana trenutnog mjeseca");
     let sedmice = document.getElementsByClassName("sedmica");
     let sedmica = sedmice[0].getElementsByTagName("td");
     let petak = 0;
     for (let i=0; i<sedmica.length; i++) {
         if (sedmica[i].classList.contains("slobodna")) break;
         petak++;
     }
     assert.equal(petak, 4,"Ocekivano da je prvi dan mjeseca petak");
   });


   it('Test za iscrtavanje kalendara za trenutni mjesec (Novembar), uz provjeru koji je dan posljednji u mjesecu', function() {
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), new Date().getMonth());
     let dani = document.getElementsByClassName("dan");
     let broj = 0;
     let subota = 0;
     for (let i=0; i<dani.length; i++) {
         if (dani[i].classList.contains("slobodna"))
            broj++;
     }

     let sedmice = document.getElementsByClassName("sedmica");
     let sedmica = sedmice[4].getElementsByTagName("td");

     for (let i=0; i<sedmica.length; i++) {
         if (sedmica[i].classList.contains("slobodna")) subota++;
         else break;
     }

     assert.equal(broj, 30,"Ocekivano 30 dana trenutnog mjeseca");
     assert.equal(subota, 6,"Ocekivano da je posljednji dan mjeseca subota");
   });


   it('Test za iscrtavanje kalendara za mjesec Januar', function() {
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 0);
     let dani = document.getElementsByClassName("dan");
     let broj = 0;
     for (let i=0; i<dani.length; i++) {
         if (dani[i].classList.contains("slobodna"))
            broj++;
     }
     assert.equal(broj, 31,"Prvi dan treba poceti u utorak");
   });


   it('Test za iscrtavanje kalendara za mjesec Februar (28 dana)', function() {
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 1);
     let dani = document.getElementsByClassName("dan");
     let broj = 0;
     for (let i=0; i<dani.length; i++) {
         if (dani[i].classList.contains("slobodna"))
            broj++;
     }
     assert.equal(broj, 28,"Ocekivano je da mjesec ima 28 dana za ovu godinu");
   });


   it('Test za iscrtavanje kalendara za mjesec Juli, gdje je prvi dan mjeseca ponedeljak', function() {
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 6);
     let dani = document.getElementsByClassName("dan");
     let broj = 0;
     for (let i=0; i<dani.length; i++) {
         if (dani[i].classList.contains("slobodna"))
            broj++;
     }
     assert.equal(broj, 31,"Ocekivano je da mjesec ima 31 dana");

     let sedmice = document.getElementsByClassName("sedmica");
     let sedmica = sedmice[0].getElementsByTagName("td");
     let ponedeljak = 0;
     for (let i=0; i<sedmica.length; i++) {
         if (sedmica[i].classList.contains("slobodna")) break;
         ponedeljak++;
     }
     assert.equal(ponedeljak, 0,"Ocekivano da je prvi dan mjeseca ponedeljak");
   });


   it('Test duplog poziva iscrtavanja trenutnog mjeseca, potrebno je biti 30 dana', function() {
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), new Date().getMonth());
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), new Date().getMonth());
     let dani = document.getElementsByClassName("dan");
     let broj = 0;
     for (let i=0; i<dani.length; i++) {
         if (dani[i].classList.contains("slobodna"))
            broj++;
     }
     assert.equal(broj, 30,"Broj dana treba biti 30");
   });

 });




 describe('obojiZauzeca()', function() {

    it('Test obojiZauzeca kada nema pozvanih podataka, ne treba se nista oznaciti kao zauzeto', function() {
     Kalendar.iscrtajKalendar(document.getElementById("kalendar"), new Date().getMonth());
     Kalendar.obojiZauzeca(document.getElementById("kalendar"), new Date().getMonth(), "MA", "12:00", "14:00");
      let dani = document.getElementsByClassName("dan");
      let broj = 0;
      for (let i=0; i<dani.length; i++) {
          if (dani[i].classList.contains("zauzeta"))
             broj++;
      }
      assert.equal(broj, 0,"Treba biti nula polja obojenih u crveno, odnosno zauzetih");
    });


    it('Test obojiZauzeca kada se dva puta ucitavaju isti podaci i vrsi zauzece, ocekivano je da se oboji bez obzira na duple vrijednosti', function() {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), new Date().getMonth());
        Kalendar.ucitajPodatke(
            [
            {dan: 2,
            semestar: "zimski",
            pocetak: "12:00",
            kraj: "13:00",
            naziv: "0-01",
            predavac: "Ime1"}, 
            ], []);
        Kalendar.obojiZauzeca(document.getElementById("kalendar"), new Date().getMonth(), "0-01", "12:00", "13:00");
        let brojDanaDuplo = document.getElementsByClassName("zauzeta").length;
        Kalendar.ucitajPodatke([{dan: 2,
            semestar: "zimski",
            pocetak: "12:00",
            kraj: "13:00",
            naziv: "0-01",
            predavac: "Ime1"}], []);
        Kalendar.obojiZauzeca(document.getElementById("kalendar"), new Date().getMonth(), "0-01", "12:00", "13:00");
        let brojDanaJednostruko = document.getElementsByClassName("zauzeta").length;
        assert.equal(brojDanaDuplo, brojDanaJednostruko,"Isti broj dana zauzeti.");
       });


       it('Test obojiZauzeca kada se ucita mjesec jednog semestra, a periodicni podaci za drugi semestar', function() {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 3);
        Kalendar.ucitajPodatke(
            [
            {dan: 2,
            semestar: "zimski",
            pocetak: "12:00",
            kraj: "13:00",
            naziv: "0-01",
            predavac: "Ime1"}, 
            ], []);
        Kalendar.obojiZauzeca(document.getElementById("kalendar"), 3, "0-01", "12:00", "13:00");
        let brojZauzetihDana = document.getElementsByClassName("zauzeta").length;
        assert.equal(0, brojZauzetihDana,"Ne treba biti zauzetih dana");
       });


       it('Test obojiZauzeca kada se postoji zauzece termina, ali u drugom mjesecu', function() {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 9);
        Kalendar.ucitajPodatke(
            [], [
                {   datum: "13.01.2019",
                    pocetak: "12:00",
                    kraj: "13:00",
                    naziv: "0-01",
                    predavac: "Ime1"}
            ]);
        Kalendar.obojiZauzeca(document.getElementById("kalendar"), 9, "0-01", "12:00", "13:00");
        let broj = 0;
        let dani = document.getElementsByClassName("dan");
        for (let i=0; i<dani.length; i++) {
            if (dani[i].classList.contains("slobodna") && dani[i].innerText === "13") break;
            broj++
        }
        assert.equal(broj, 13,"Ne treba biti zauzet 13. u mjesecu");
       });


       it('Test obojiZauzeca kada je potrebno obojiti sve dane u mjesecu tako da su zauzeti', function() {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), new Date().getMonth());
        Kalendar.ucitajPodatke(
            [
            {dan: 0,
            semestar: "zimski",
            pocetak: "12:00",
            kraj: "13:00",
            naziv: "0-01",
            predavac: "Ime1"},
            {dan: 1,
                semestar: "zimski",
                pocetak: "12:00",
                kraj: "13:00",
                naziv: "0-01",
                predavac: "Ime1"},  
                {dan: 2,
                    semestar: "zimski",
                    pocetak: "12:00",
                    kraj: "13:00",
                    naziv: "0-01",
                    predavac: "Ime1"}, 
                    {dan: 3,
                        semestar: "zimski",
                        pocetak: "12:00",
                        kraj: "13:00",
                        naziv: "0-01",
                        predavac: "Ime1"}, 
                        {dan: 4,
                            semestar: "zimski",
                            pocetak: "12:00",
                            kraj: "13:00",
                            naziv: "0-01",
                            predavac: "Ime1"}, 
                            {dan: 5,
                                semestar: "zimski",
                                pocetak: "12:00",
                                kraj: "13:00",
                                naziv: "0-01",
                                predavac: "Ime1"}, 
                                {dan: 6,
                                    semestar: "zimski",
                                    pocetak: "12:00",
                                    kraj: "13:00",
                                    naziv: "0-01",
                                    predavac: "Ime1"}, 
            ], []);
        Kalendar.obojiZauzeca(document.getElementById("kalendar"), new Date().getMonth(), "0-01", "12:00", "13:00");
        let brojZauzetihDana = document.getElementsByClassName("zauzeta").length;
        assert.equal(30, brojZauzetihDana,"Ne treba biti zauzetih dana");
       });


       it('Test obojiZauzeca gdje se dva puta uzastopno poziva funkcije, ne treba biti promjena', function() {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 2);
        Kalendar.ucitajPodatke(
            [], [
                {   datum: "13.03.2019",
                    pocetak: "12:00",
                    kraj: "13:00",
                    naziv: "0-01",
                    predavac: "Ime1"}
            ]);
        Kalendar.obojiZauzeca(document.getElementById("kalendar"), 2, "0-01", "12:00", "13:00");
        let brojZauzetihDana1 = document.getElementsByClassName("zauzeta").length;
        Kalendar.obojiZauzeca(document.getElementById("kalendar"), 2, "0-01", "12:00", "13:00");
        let brojZauzetihDana2 = document.getElementsByClassName("zauzeta").length;
        assert.equal(brojZauzetihDana1, brojZauzetihDana2,"Treba sve ostati isto kao i nakon prvog poziva funkcije");
       });


       it('Test obojiZauzeca gdje se dva puta poziva ucitavanje podataka, potrebno je registrovati samo prvu promjenu', function() {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 2);
        Kalendar.ucitajPodatke(
            [], [
                {   datum: "13.03.2019",
                    pocetak: "12:00",
                    kraj: "13:00",
                    naziv: "0-01",
                    predavac: "Ime1"}
            ]);
        Kalendar.obojiZauzeca(document.getElementById("kalendar"), 2, "0-01", "12:00", "13:00");
        Kalendar.ucitajPodatke(
            [], [
                {   datum: "20.03.2019",
                    pocetak: "12:00",
                    kraj: "13:00",
                    naziv: "0-01",
                    predavac: "Ime1"}
            ]);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 2, "0-01", "12:00", "13:00");
            let brojZauzetihDana2 = document.getElementsByClassName("zauzeta").length;
        assert.equal(1, brojZauzetihDana2,"treba samo 20 biti zauzet, ne i 13");
       });


       it('Test obojiZauzeca gdje se pogresno vrijeme unese u ucitajPodatke, nista ne treba obojiti', function() {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 2);
        Kalendar.ucitajPodatke(
            [], [
                {   datum: "13.03.2019",
                    pocetak: "1a2:00",
                    kraj: "13:00",
                    naziv: "0-01",
                    predavac: "Ime1"}
            ]);
        Kalendar.obojiZauzeca(document.getElementById("kalendar"), 2, "0-01", "12:00", "13:00");
            let brojZauzetihDana2 = document.getElementsByClassName("zauzeta").length;
        assert.equal(0, brojZauzetihDana2,"Nista ne treba biti popunjeno");
       });


       it('Test obojiZauzeca gdje se pogresan naziv sale unese u ucitajPodatke, nista ne treba obojiti', function() {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 2);
        Kalendar.ucitajPodatke(
            [], [
                {   datum: "13.03.2019",
                    pocetak: "12:00",
                    kraj: "13:00",
                    naziv: "00-01",
                    predavac: "Ime1"}
            ]);
        Kalendar.obojiZauzeca(document.getElementById("kalendar"), 2, "0-01", "12:00", "13:00");
            let brojZauzetihDana2 = document.getElementsByClassName("zauzeta").length;
        assert.equal(0, brojZauzetihDana2,"Nista ne treba biti popunjeno");
       });

 });

});
