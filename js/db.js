const Sequelize = require("sequelize");
const sequelize = new Sequelize("DBWT19", "root", "root", {
   host: "127.0.0.1",
   dialect: "mysql",
   logging: false
});

const Osoblje = sequelize.import(__dirname+"/osoblje.js");
const Termin = sequelize.import(__dirname+"/termin.js");
const Sala = sequelize.import(__dirname+"/sala.js");
const Rezervacija = sequelize.import(__dirname+"/rezervacije.js");


Osoblje.hasOne(Sala, { foreignKey: 'zaduzenaOsoba' });
Osoblje.hasMany(Rezervacija, { foreignKey: 'osoba' });
Termin.hasOne(Rezervacija, { foreignKey: {name:'termin', unique: true}});
Sala.hasMany(Rezervacija, { foreignKey: 'sala' });


sequelize.drop().then(() => Osoblje.sync().then( () =>  
   Termin.sync().then( () =>
      Sala.sync().then( () =>
         Rezervacija.sync().then(popuniPocetne
         , err => console.log(err)
         ),err => console.log(err)
         ),err => console.log(err)
         ),err => console.log(err)));

module.exports = sequelize;

async function popuniPocetne() {
await Osoblje.create({ime: "Neko", prezime: "Nekic", uloga: "profesor"});
await Osoblje.create({ime: "Drugi", prezime: "Neko", uloga: "asistent"});
await Osoblje.create({ime: "Test", prezime: "Test", uloga: "asistent"});
await Sala.create({naziv: "1-11", zaduzenaOsoba: 1});
await Sala.create({naziv: "1-15", zaduzenaOsoba: 2});
await Termin.create({redovni: false, dan: null, datum: "01.01.2020", semestar: null, pocetak: new Date(2020, 1, 1, 13, 0, 0, 0), kraj: new Date(2020, 1, 1, 14, 0, 0, 0)});
await Termin.create({redovni: true, dan: 0, datum: null, semestar: "zimski", pocetak: new Date(2020, 1, 1, 14, 0, 0, 0), kraj: new Date(2020, 1, 1, 15, 0, 0, 0)}); 
await Rezervacija.create({termin: 1, sala: 1, osoba: 1});
await Rezervacija.create({termin: 2, sala: 1, osoba: 3});
}